Img-gallery
===========

Django application to display an image gallery.

Features:
- Import all images located in the `data` folder
- List all images on the main page, with pagination
- Click on a image to access the image page a see a bigger version of the image, with comment


## Installation

```bash
python3 -m venv venv
source venv/bin/activate
pip install requirements.txt
yarn install
python manage.py migrate
django-admin compilemessages

# Load all images located in `data` folder in database:
python manage.py img2db
```
