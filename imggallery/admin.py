from django.contrib import admin

from imggallery.models import Image, ImagePath


admin.site.register(Image)
admin.site.register(ImagePath)
