from django.urls import path
from django.views.generic.base import TemplateView

from .views import ImageListView, ImageDetailView

urlpatterns = [
    path('', ImageListView.as_view(), name='homepage'),
    path('view/<int:pk>', ImageDetailView.as_view(), name="imageview"),
    path("robots.txt", TemplateView.as_view(
        template_name="robots.txt",
        content_type="text/plain"
    ))
]
