import glob
import os
from django.core.management.base import BaseCommand
from django.db import transaction

from imggallery.models import Image, ImagePath


class Command(BaseCommand):
    help = 'Load all images stored in data folder in database'

    def handle(self, *args, **options):
        accepted_ext = ['png', 'jpg', 'jpeg', 'tiff', 'bmp']
        for f in glob.glob('data/**/*', recursive=True):
            if os.path.isfile(f):
                if f.split('.')[-1].lower() in accepted_ext:
                    with transaction.atomic():
                        img = Image()
                        img.save()

                        img_path = ImagePath()
                        img_path.image = img
                        img_path.filepath = f
                        img_path.save()

                    print(f + " saved")
        self.stdout.write(self.style.SUCCESS('Successfully done.'))
