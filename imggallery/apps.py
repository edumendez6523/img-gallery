from django.apps import AppConfig


class ImggalleryConfig(AppConfig):
    name = 'imggallery'
