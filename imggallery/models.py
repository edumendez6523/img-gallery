from django.db import models


class Image(models.Model):
    comment = models.TextField(blank=True)

    def main_path(self):
        return self.imagepath_set.first().filepath


class ImagePath(models.Model):
    filepath = models.ImageField(upload_to="data/", max_length=255)
    image = models.ForeignKey(Image, on_delete=models.CASCADE)
