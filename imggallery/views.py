from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from imggallery.models import Image


class ImageListView(ListView):
    template_name = 'image_list.html'
    model = Image
    queryset = Image.objects.all().order_by('pk').prefetch_related(
        'imagepath_set'
    )
    paginate_by = 60


class ImageDetailView(DetailView):
    template_name = "image_detail.html"
    model = Image

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        referer = self.request.META.get('HTTP_REFERER')
        if referer is None or \
                not referer.startswith(self.request.build_absolute_uri('/')):
            context['back_url'] = reverse_lazy('homepage')
        else:
            context['back_url'] = referer

        return context
